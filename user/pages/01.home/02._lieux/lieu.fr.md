---
title: "Découverte de la semaine"
show_titre: true
---

Berlin est l’un des temples européens de l’exploration urbaine. Je m’y suis rendue récemment, et j’en ai profité pour effectuer quelques visites qui me tenaient à cœur comme le Spreepark, et surtout cette ancienne station d’espionnage américaine.

Voici l’histoire de ce site extraordinaire, accompagnée de quelques photos…

Située à Berlin-Ouest sur la Montagne du Diable (Teufelsberg en allemand), cette ancienne station de la National Security Agency (NSA) fut construite par les alliés après la seconde guerre mondiale, à partir des gravats des ruines la ville (estimés à plus de 12.000.000 de m3, ou 400.000 bâtiments réduits en poussière, rien que cela).

Imaginez un peu : sous cette tour de 120 mètres de haut fut enterrée une université militaire nazie – trop solide pour être détruite.

C’est en l’engloutissant sous ces tonnes de débris que les alliés ont donné une nouvelle vie à la Montagne du Diable.

Il faut aussi savoir que la NSA avait construit à Berlin l’une de ses plus grandes stations.
