---
title: 'Lieux'
body_classes: "spot"
content:
    items: '@self.modular'
---

Rover.fr à pour but de créer une communauté autour de l’Exploration Urbaine, plus connue sous le nom d’ Urbex. C’est le site idéal pour découvrir des lieux oubliés aux travers d’articles et de visuels à coupés le souffle. Fait par des explorateurs pour des explorateurs.


Le site repose sur un communauté fermée de chroniqueurs et la rédaction est contrôlée avant la parution. L’idée est de proposer constament un contenu qualitatif à ses visiteurs.


Vous pouvez vous procurer une invitation au prêt de détenteurs d’invitations sur twitter et instagram. Il est souvent demandé de leur envoyer votre portfolio car le chroniqueur qui vous invite se porte garant de votre probité et des futures posts que vous publierai.