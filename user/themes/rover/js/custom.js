//LAZYLOAD
document.addEventListener("DOMContentLoaded", function(e) {
    var ll = new LazyLoad({
        elements_selector: 'div.lazy, img.lazy',
        callback_set: function(el) {
            if (el.tagName == 'DIV') {
                (!!document.body.classList) ? (el.classList.add('loaded')) : (el.className += (el.className ? ' ' : '') + 'loaded');
            }
        }
    });
    if (window.pw) {
        window.pw.finish(false, function() {
            ll.update();
        });
    }
    var scrollEvent = new Event('scroll');
    window.dispatchEvent(scrollEvent);
});


//NAVIGATION
$("button,nav").click(function() {
    $(".hamburger").toggleClass("focus");
    $(".content").toggleClass("show");
    $(".hero").toggleClass("hidden");
    $(".body").toggleClass("overflow");
});

//SWIPER
var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 70,
    // init: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    breakpoints: {
        768: {
            slidesPerView: 3,
            spaceBetween: 30,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    }
});


