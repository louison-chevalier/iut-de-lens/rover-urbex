
# ![](https://avatars1.githubusercontent.com/u/8237355?v=2&s=50) Rover : communauté exploration urbaine

## DESCRIPTION :

Rover.fr à pour but de créer une communauté autour de l’Exploration Urbaine, plus connue sous le nom d’ Urbex.

C’est le site idéal pour découvrir des lieux oubliés aux travers d’articles et de visuels à coupés le souffle. Fait par des explorateurs pour des explorateurs.

La possibilité pour un utilisateur de publier se fera sur invitation sur le principe dribbble.
