
# ![](https://avatars1.githubusercontent.com/u/8237355?v=2&s=50) Rover : communauté exploration urbaine

## DESCRIPTION :

Rover.fr à pour but de créer une communauté autour de l’Exploration Urbaine, plus connue sous le nom d’ Urbex.

C’est le site idéal pour découvrir des lieux oubliés aux travers d’articles et de visuels à coupés le souffle. Fait par des explorateurs pour des explorateurs.

La possibilité pour un utilisateur de publier se fera sur invitation sur le principe dribbble.

## TYPOGRAPHIE :

Pour la réalisation du site Rover, la charte graphique impose d'utiliser les polices suivantes :
- Macbeth
- Helvetica


## COULEURS :

Pour la réalisation du site Rover, la charte graphique impose d'utiliser les couleurs suivantes :
- #FFFFFF
- #000000


## GRAV FRAMEWORK :
Grav est une plate-forme Web basée sur des fichiers **rapide**, **simple** et **flexible**.

Il suit des principes similaires à ceux des autres plates-formes CMS à fichier plat, mais présente une philosophie de conception différente de la plupart des autres.

Grav est livré avec un puissant **système de gestion de paquets** qui permet une installation et une mise à niveau simples des plugins et des thèmes, ainsi qu'une simple mise à jour de Grav lui-même.

L’architecture sous-jacente de Grav est conçue pour utiliser des technologies bien établies et optimisées afin de garantir que Grav est simple à utiliser et à étendre. Certaines de ces technologies clés incluent:

* [Twig Templating](http://twig.sensiolabs.org/): pour un contrôle puissant de l'interface utilisateur
* [Markdown](http://en.wikipedia.org/wiki/Markdown): pour une création de contenu facile
* [YAML](http://yaml.org): pour une configuration simple
* [Parsedown](http://parsedown.org/): pour une prise en charge rapide de Markdown et Markdown Extra
* [Doctrine Cache](http://doctrine-orm.readthedocs.io/projects/doctrine-orm/en/latest/reference/caching.html): couche pour la performance
* [Pimple Dependency Injection Container](http://pimple.sensiolabs.org/): pour l'extensibilité et la maintenabilité
* [Symfony Event Dispatcher](http://symfony.com/doc/current/components/event_dispatcher/introduction.html): pour la gestion des événements du plugin
* [Symfony Console](http://symfony.com/doc/current/components/console/introduction.html): pour interface CLI
* [Gregwar Image Library](https://github.com/Gregwar/Image): pour la manipulation d'image dynamique

### Requirements

- PHP 5.6.4 ou supérieur. Vérifier la liste des modules requis [required modules list](https://learn.getgrav.org/basics/requirements#php-requirements)
- Vérifiez les exigences Apache ou IIS [Apache](https://learn.getgrav.org/basics/requirements#apache-requirements) or [IIS](https://learn.getgrav.org/basics/requirements#iis-requirements) requirements


### Espace administrateur Grav :

**L'accès à l'espace administrateur se fait depuis :**
- http://site/fr/admin

**Login pour accéder à l'espace administrateur :**
- **Pseudo** : louisonchevalier
- **Mot de passe :** Rig9@~'$R^M#Mi]:

### Grav License

Voir [LICENSE](LICENSE.txt)

[gitflow-model]: http://nvie.com/posts/a-successful-git-branching-model/
[gitflow-extensions]: https://github.com/nvie/gitflow
